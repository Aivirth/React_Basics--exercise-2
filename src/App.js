import React, { Component } from "react";
import Instructions from "./components/Instructions";
import Validation from "./components/Validation";
import Char from "./components/Char";
import "./App.css";

class App extends Component {
  state = {
    textField: "test"
  };

  onChangeTextField = event => {
    const newState = [...this.state];

    newState.textField = event.target.value;

    this.setState({
      textField: newState.textField
    });
  };

  deleteLetter = index => {
    const text = this.state.textField.split("");
    text.splice(index, 1);
    const updatedText = text.join("");
    this.setState({
      textField: updatedText
    });
  };

  render() {
    const textFieldLetters = this.state.textField.split("");
    const { textField } = this.state;
    return (
      <div className="App">
        <Instructions />

        <input
          type="text"
          name="textField"
          value={textField}
          onChange={this.onChangeTextField}
        />
        <p>{this.state.textField.length}</p>

        <Validation textLength={this.state.textField.length} />

        {textFieldLetters.map((letter, index) => (
          <Char
            letter={letter}
            key={index}
            clicked={() => this.deleteLetter(index)}
          />
        ))}
      </div>
    );
  }
}

export default App;
