import React from "react";

export default props => {
  const textLength = props.textLength;

  return (
    <div>
      {textLength < 5 ? <p>Text too short</p> : <p>Text long enough</p>}
    </div>
  );
};
